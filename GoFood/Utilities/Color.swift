//
//  Color.swift
//  GoFood
//
//  Created by Newarpunk on 8/16/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

enum Color {
    static let defaultColor = UIColor(red: 230/255, green: 32/255, blue: 32/255, alpha: 1)
    static let fbColor = UIColor(red: 59/255, green: 89/255, blue: 153/255, alpha: 1)
    static let twitterColor = UIColor(red: 0/255, green: 172/255, blue: 238/255, alpha: 1)
}
