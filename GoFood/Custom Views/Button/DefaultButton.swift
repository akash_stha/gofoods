import UIKit

class DefaultButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configButton()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configButton()
    }
    
    func configButton() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        self.titleLabel?.font = .boldSystemFont(ofSize: 16.0)
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = Color.defaultColor
    }

}
