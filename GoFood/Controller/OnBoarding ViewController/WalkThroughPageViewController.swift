//
//  WalkThroughPageViewController.swift
//  GoFood
//
//  Created by Newarpunk on 8/16/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

protocol WalkThroughPageViewControllerDelegate: class {
    func didUpdatePageIndex(currentIndex: Int)
}

class WalkThroughPageViewController: UIPageViewController {
    
    // MARK:- Properties
    var pageHeadings = ["Quick Search","Variety of food","Search for a place","Fast shipping"]
    var pageAnimation = ["quick","noodels","location","food-delivery-boy"]
    var pageSubHeadings = ["Set your location to start exploring restaurants around you","Set your location to start exploring restaurants around you","Set your location to start exploring restaurants around you","Set your location to start exploring restaurants around you"]
    
    var currentIndex = 0
    
    weak var walkThroughDelegate: WalkThroughPageViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        if let startingViewController = contentViewController(at: 0) {
            setViewControllers([startingViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    

    // MARK: - Functions
    func contentViewController(at index: Int) -> WalkThroughContentViewController? {
        if index < 0 || index >= pageHeadings.count {
            return nil
        }
        
        // Create new viewcontroller amd pass suitable data
        if let pageContentViewController = storyboard?.instantiateViewController(withIdentifier: "WalkThroughContentViewController") as? WalkThroughContentViewController {
            pageContentViewController.animationFiles = pageAnimation[index]
            pageContentViewController.heading = pageHeadings[index]
            pageContentViewController.subHeading = pageSubHeadings[index]
            pageContentViewController.index = index
            
            return pageContentViewController
        }
        return nil
    }
    
    func forwardPage() {
        currentIndex += 1
        if let nextViewController = contentViewController(at: currentIndex) {
            setViewControllers([nextViewController], direction: .forward, animated: true, completion: nil)
        }
    }

}

extension WalkThroughPageViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! WalkThroughContentViewController).index
        index -= 1
        return contentViewController(at: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! WalkThroughContentViewController).index
        index += 1
        return contentViewController(at: index)
    }
    
    // MARK:- Page View Controller Delegate
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let contentViewController = pageViewController.viewControllers?.first as? WalkThroughContentViewController {
                currentIndex = contentViewController.index
                walkThroughDelegate?.didUpdatePageIndex(currentIndex: currentIndex)
            }
        }
    }
    
}
