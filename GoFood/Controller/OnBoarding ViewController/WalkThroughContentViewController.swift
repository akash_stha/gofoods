//
//  WalkThroughContentViewController.swift
//  GoFood
//
//  Created by Newarpunk on 8/16/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit
import Lottie

class WalkThroughContentViewController: UIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var animationView: AnimationView!
    
    // MARK:- Variables
    var index = 0
    var heading = ""
    var subHeading = ""
    var animationFiles = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLabel.text = heading
        self.subHeadingLabel.text = subHeading
        startAnimation()
    }
    
    func startAnimation() {
        self.animationView.animation = Animation.named(animationFiles)
        self.animationView.loopMode = .loop
        self.animationView.play()
    }


}
