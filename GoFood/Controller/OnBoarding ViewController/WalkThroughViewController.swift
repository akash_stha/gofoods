//
//  WalkThroughViewController.swift
//  GoFood
//
//  Created by Newarpunk on 8/16/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class WalkThroughViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: DefaultButton!
    @IBOutlet weak var skipButton: UIButton!
    
    // MARK: - Properties
    weak var walkThroughPageViewController: WalkThroughPageViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- Functions
    func updateUI() {
        if let index = walkThroughPageViewController?.currentIndex {
            switch index {
            case 0...2:
                nextButton.setTitle("Next", for: .normal)
                skipButton.isHidden = false
            case 3:
                nextButton.setTitle("Get Started", for: .normal)
                skipButton.isHidden = true
            default:
                break
            }
            pageControl.currentPage = index
        }
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        if let index = walkThroughPageViewController?.currentIndex {
            switch index {
            case 0...2:
                walkThroughPageViewController?.forwardPage()
            case 3:
                print("i am here")
//                let tabBarVC: TabBarViewController = TabBarController.instantiate(name: "TabBar")
//                self.view.window?.rootViewController = tabBarVC
            default:
                break
            }
        }
        updateUI()
    }
    
    @IBAction func skipButtonClicked(_ sender: UIButton) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let pageViewController = destination as? WalkThroughPageViewController {
            walkThroughPageViewController = pageViewController
            walkThroughPageViewController?.walkThroughDelegate = self
        }
    }
}

extension WalkThroughViewController: WalkThroughPageViewControllerDelegate {
    func didUpdatePageIndex(currentIndex: Int) {
        updateUI()
    }
}
